'use strict'

module.exports = {
  plugins: [],
  tags: {
    allowUnknownTags: true
  },
  source: {
    include: ['./app/site/lib'],
    includePattern: '.+\\.js(doc|x)?$',
    excludePattern: '(^|\\/|\\\\)_'
  },
  templates: {
    cleverLinks: false,
    monospaceLinks: false,
    default: {
      outputSourceFiles: true
    }
  },
  recurseDepth: 5,
  opts: {
    // template: 'templates/default', // same as -t templates/default
    encoding: 'utf8',
    destination: './docs',
    recurse: true
    // tutorials: 'path/to/tutorials'
  }
}
