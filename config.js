'use strict'

module.exports = {
  ip: 'localhost',
  port: process.env.PORT || process.argv[2] || 1337
}
