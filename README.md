# point-and-click

Author: Tom DE RUDDER  
Version `2.0.0`

---

> "_point-and-click_" est une librairie JavaScript permettant de crée des jeux point & click.

## Introduction

La librairie est codez en Javascript, en es6, elle est structuré en classes. Cette derniére est acompagné d'un script démo.

### Features

_point-and-click_ permet:

- gestion audio (bruits pas, son d'ambiance, mise en sourdine)
- gestion de scénes et de calques
- gestion des deplacement du joueur (zone de deplacement, recherche de chemin)
- gestion de zone d'action
- gestion du curseur (adaptatif en fonction des bords du cadre)
- stockage de données dans les cookies
- animation de personnages
- animation des deplacemnt de la camera
- dialogue avec les personnages

## Improvements

_point-and-click_ permet pas encore:

- gestion du volume sonore
- gestion des objet et de l'inventaire
- gestion des deplacement des personnages non-joueur
- deplacement sur l'axe z
- gestion du cursor pour bien centrer la pointe (left top etc)
- stocker le cursor ds un sprite
- separer la method draw en  2 partie
- stocker la position initial des personnage dans la scene est plus dans les personnages
- revoir els audios des character

## Outbuildings

- [serve](https://www.npmjs.com/package/serve)

## Install

```bash
git clone "https://gitlab.com/tomderudderetna/point-and-click.git" point-and-click
cd point-and-click/app/
npm i -g serve
```

## Usage

### Start

```bash
serve site
```

## Documentation

<a href="./docs/index.html" target="_blank">go to doc url</a>

## Structure

```bash
	point-and-click
	├── misc /
	│
	├── site
	│	├── public
	│	│	├── assets
	│	│	│	├── audio / # sounds & musics
	│	│	│	├── css / # style
	│	│	│	├── img / # sprites & background images
	│	│	│	├── js /
	│	│	│	└── favicon.ico
	│	│	|
	│	│	├── lib / # application code & classes
	│	│	└── index.html
	│	│
	│	├── app.js
	│	└── package.json
	│
	│
	└── README.md
```

## Contributing

Please refer to our [Contribution Guide](contributing.md)

## Logo

![logo](app/misc/logo/logo.png)

<!--
## Bugs

bug 1 description

- [link 1](url)
- [link 2](url)
- [link 3](url)
- [link n](url)

bug 2 description

- [link](url) -->

## Useful links

- [Fullscreen API has been unprefixed](https://www.fxsitecompat.dev/en-CA/docs/2018/fullscreen-api-has-been-unprefixed)

- [sprite](<https://fr.wikipedia.org/wiki/Sprite_(jeu_vid%C3%A9o)>)
- [Modify prototypes of every possible DOM element](https://stackoverflow.com/questions/3653818/modify-prototypes-of-every-possible-dom-element)
- [a](https://stackoverflow.com/questions/30044647/javascript-element-requestfullscreen-is-undefined/30044770)
- [a](https://developer.mozilla.org/fr/docs/Web/API/Document/onfullscreenchange)
- [a](https://stackoverflow.com/questions/14549547/fullscreenchange-event-not-work)
- [a](https://www.w3schools.com/jsref/prop_screen_height.asp)
- [a](https://developer.mozilla.org/fr/docs/Web/Guide/DOM/Using_full_screen_mode)
- [Function.name
  ](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Function/name)
- [HTML5 audio
  ](https://en.wikipedia.org/wiki/HTML5_audio#cite_note-14)
- [HTMLAudioElement
  ](https://developer.mozilla.org/en-US/docs/Web/API/HTMLAudioElement)
- [Detect when an image fails to load in Javascript](https://stackoverflow.com/questions/9815762/detect-when-an-image-fails-to-load-in-javascript)
- [List of common resolutions
  ](https://en.wikipedia.org/wiki/List_of_common_resolutions)
- [HTML5 canvas full-screen and full-page
  ](http://h3manth.com/content/html5-canvas-full-screen-and-full-page)

<!-- ## Technical debt (`optional`)

- debt 1
- debt 2
- debt 3
- debt n

## Improvements (`optional`)

- improvement 1
- improvement 2
- improvement 3
- improvement n

## Notes (`optional`)

```
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nec sollicitudin nibh. Mauris eu finibus ipsum. Aliquam commodo, ipsum sit amet sodales pellentesque, enim arcu maximus leo, vitae molestie nisl nunc sed ante.
``` -->
