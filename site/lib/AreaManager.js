'use strict'

const AREA_SCALE = 10
const WALKABLE_AREA_COLOR = 'rgba(255, 0, 255, .5)'
const SWITCHABLE_AREA_COLOR = 'rgba(0, 255, 255, .5)'

/**
 * Manage areas in the application.
 * @param {number} width - The width of the scene.
 * @param {number} height - The height of the scene.
 */
class AreaManager {
  /**
   * @constructor
   * @param {number} width - The width of the scene.
   * @param {number} height - The height of the scene.
   */
  constructor(width, height) {
    /** list of areas for the player walk in the scene*/
    this.walkableAreas = []
    /** list of areas for the player swit between two scene*/
    this.switchableAreas = []
    this.initalizeMatrix(width, height)
    this.finder = new PF.AStarFinder()
  }

  /**
   * Return all areas.
   */
  get areas() {
    return this.switchableAreas.concat(this.walkableAreas)
  }

  /**
   * Initalize the matrix
   * @param {number} width - The width of the scene.
   * @param {number} height - The height of the scene.
   */
  initalizeMatrix(width, height) {
    this.matrix = []
    for (let y = 0; y < height / AREA_SCALE; y++) {
      this.matrix[y] = []
      for (let x = 0; x < width / AREA_SCALE; x++) {
        this.matrix[y][x] = 1
      }
    }
  }

  /**
   * Return all areas contains a specified position.
   * @param {Vector} position - The position to seek.
   */
  getActiveArea(position) {
    let activeAreas = []
    for (const area of this.areas) if (area.contains(position)) activeAreas.push(area)

    return activeAreas
  }

  /**
   * Find a walkable path between two positions.
   * @param {Vector} start - The initial position.
   * @param {Vector} end - The finale position.
   * @param {function} callback - The callback execute at the end.
   */
  calculatePath(start, end, callback) {
    start = new Vector(parseInt(start.x / AREA_SCALE), parseInt(start.y / AREA_SCALE))
    end = new Vector(parseInt(end.x / AREA_SCALE), parseInt(end.y / AREA_SCALE))
    const grid = new PF.Grid(this.matrix)
    const path = this.finder.findPath(start.x, start.y, end.x, end.y, grid)
    callback(path)
  }

  /**
   * Add a area to manage.
   * @param {Area} area - The area.
   */
  add(area) {
    if (area) {
      switch (area.type) {
        case WALKABLE_AREA:
          this.walkableAreas.push(area)
          switch (area.shapeType) {
            case AREA_RECT:
              const areaX = parseInt(area.coordinates.x / AREA_SCALE)
              const areaY = parseInt(area.coordinates.y / AREA_SCALE)
              for (let y = areaY; y < (area.coordinates.y + area.coordinates.h) / AREA_SCALE; y++)
                for (
                  let x = areaX;
                  x < (area.coordinates.x + area.coordinates.w) / AREA_SCALE;
                  x++
                ) {
                  this.matrix[y][x] = 0
                }
              break
            case AREA_CIRCLE:
              break
          }
          break

        case SWITCHABLE_AREA:
          this.switchableAreas.push(area)
          break
      }
    }
  }

  /**
   * Draw a walkable areas and the switchable areas.
   * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
   */
  draw(ctx) {
    if (!CONF_HIDE_AREA) {
      const oldColor = ctx.fillStyle
      this.drawAreas(ctx, this.walkableAreas, WALKABLE_AREA_COLOR)
      this.drawAreas(ctx, this.switchableAreas, SWITCHABLE_AREA_COLOR)
      ctx.fillStyle = oldColor
    }
  }

  /**
   * Draw areas.
   * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
   * @param {array} areas - The array of areas to draw.
   * @param {string} color - The fill color.
   */
  drawAreas(ctx, areas, color) {
    ctx.beginPath()
    ctx.fillStyle = color
    areas.forEach(area => area.draw(ctx))
    ctx.fill()
  }

  /**
   * Get the closest position to a initial position
   * @param {Vector} position - The inital position.
   */
  getPositionClosest(position) {
    const x = parseInt(position.x / AREA_SCALE)
    const initalY = parseInt(position.y / AREA_SCALE)
    let y = initalY
    while (this.matrix[y][x] !== 0 && y < this.matrix.length - 1) y++
    return new Vector(position.x, y * AREA_SCALE)
  }
}
