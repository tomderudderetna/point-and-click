'use strict'

/**
 * Represents a Vector.
 * @param {number} x - The position on the x-axis.
 * @param {number} y - The position on the y-axis.
 * @param {number} z - The position on the z-axis.
 * @return {Vector} The object create
 */

class Vector {
  /**
   * @constructor
   * @param {number} x - The position on the x-axis.
   * @param {number} y - The position on the y-axis.
   * @param {number} z - The position on the z-axis.
   * @return {Vector} The object create
   */

  constructor(x = 0, y = 0, z = 0) {
    this.x = x
    this.y = y
    this.z = z

    return this
  }

  /**
   * @name calculateDirection
   * @param {Vector} from source
   * @param {Vector} to destination
   * @return {Vector} The calculate direction
   * @static
   */

  static calculateDirection(from, to) {
    return new Vector(
      from.x - to.x < 0 ? 1 : -1, // right or left
      from.y - to.y < 0 ? 1 : -1 // bottom or top
    )
  }
}
