'use strict'

// TODO
const CHARACTER_SPEED = 3
const CHARACTER_SPEED2 = 2

/**
 * Represents a character.
 * @param {string} name - The name of the character.
 * @param {[Sprite,AnimatedSprite]} component - The character.
 * @param {object} options
 */

class Character {
  /**
   * @constructor
   * @param {string} name - The name of the character.
   * @param {[Sprite,AnimatedSprite]} component - The character.
   * @param {object} options
   */

  constructor(name = 'character ' + Character.characterCount, component, options) {
    this.name = name
    this.component = component
    this.initialPosition = { ...component.position }
    this.zIndex = 99
    this.isDrawn = false
    this.destination = null
    this.intervalId = null
    this.text = null
    this._path = []
    this.audios = {
      walk: null, // footstep
      climb: null // ladder sound
    }
    Character.characterCount++
    this.movePath = this.movePath.bind(this)

    // load options
    if (options) {
      const { zIndex, audios } = options

      if (zIndex) {
        this.zIndex = zIndex
      }
      if (audios) {
        for (let i = 0; i < audios.length; i++) {
          this.addAudio(audios[i])
        }
      }
    }
  }

  /**
   * Return the position.
   */
  get position() {
    return this.realPosition(this.component.position)
  }

  set position(p) {
    this.component.position = this.realDestination(p)
  }

  /**
   * Bind a externe method to the object.
   * @param {function} method - The method to bind.
   */
  bind(method) {
    return event => this[method.name](event)
  }

  /**
   * Return the position of the bottom of the sprite.
   * @param {Vector} position - The initial position.
   */
  realPosition(position) {
    return new Vector(
      position.x + this.component.width / 2,
      position.y + this.component.height - 25
    )
  }

  /**
   * Return the position to the bottom of the sprite for move.
   * @param {Vector} destination - The initial destination.
   */
  realDestination(destination) {
    return new Vector(
      destination.x - this.component.width / 2,
      destination.y - this.component.height + 25
    )
  }

  /**
   * @name movePath
   */
  movePath() {
    if (this.path.length > 0) {
      const from = this.position
      const to = new Vector(this.path[0][0], this.path[0][1])

      // move
      this.component.direction = Vector.calculateDirection(from, to)
      this.component.speed = new Vector(CHARACTER_SPEED, CHARACTER_SPEED)

      // animate sprite
      this.component['animeMove' + (from.x - to.x > 0 ? 'Left' : 'Right')]()
      this.audios.walk.play()

      // stop
      const deltaX = Math.abs(from.x - to.x)
      const deltaY = Math.abs(from.y - to.y)

      if (deltaX <= CHARACTER_SPEED) {
        this.component.speed.x = 0
      }
      if (deltaY <= CHARACTER_SPEED) {
        this.component.speed.y = 0
      }
      if (deltaX <= CHARACTER_SPEED && deltaY <= CHARACTER_SPEED) {
        // one step finish
        this.path.shift()
      }

      setTimeout(() => {
        this.movePath()
      }, CHARACTER_SPEED2)
    } else {
      // all steps finish
      this.stop()
    }
  }

  /**
   * Move the character to a position.
   * @param {Vector} destination - The destination.
   */
  moveTo(destination) {
    console.log(destination)
  }

  /**
   * Move the character.
   */
  // move() {
  //   console.log('walk')

  //   const deltaX = Math.abs(this.component.position.x - this.destination.x)
  //   const deltaY = Math.abs(this.component.position.y - this.destination.y)

  //   if (deltaX < 5) {
  //     this.component.speed.x = 0
  //   }
  //   if (deltaY < 5) {
  //     this.component.speed.y = 0
  //   }
  //   if (deltaX < 5 && deltaY < 5) {
  //     this.path.shift()
  //     if (this.path.length) {
  //       this.moveTo(new Vector(this.path[0][0] * AREA_SCALE, this.path[0][1] * AREA_SCALE))
  //     } else {
  //       this.stop()
  //     }
  //     // this.component = new Sprite('img/men.png', this.component.x, this.component.y, 98, 300);
  //   }
  // }

  /**
   * Stop to move the character.
   */
  stop() {
    this._path = []
    // clearInterval(this.intervalId)
    this.component.speed = new Vector(0, 0)
    this.audios.walk.pause()
    this.component.stop()
    this.onMoveEnd()
  }

  /**
   * Initialize the inital character position and speed.
   */
  setToInitalPosition() {
    this.stop()
    this.component.position = { ...this.initialPosition }
  }

  /**
   * Add audio to the character.
   * @param {Sound} audio - The audio to add.
   */
  addAudio(audio) {
    audio.onended = () => audio.play()
    this.audios.walk = audio
  }

  /**
   * Draw the character.
   * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
   * @param {function} ctx - The callback to execute before drawing the character.
   */
  draw(ctx, callback) {
    if (!this.isDrawn) {
      this.isDrawn = true
      callback()
      this.component.draw(ctx)
    }
  }

  /**
   * Set the text to the character.
   * @param {string} text - The text.
   */
  talk(text) {
    this.text = text
    setTimeout(() => {
      this.text = null
    }, 1500)
  }

  /**
   * Draw the text to the character.
   * @param {string} text - The text.
   * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
   */
  textDraw(ctx) {
    if (this.text)
      ctx.fillText(
        this.text,
        this.component.position.x - Scene.offsetX + this.component.width / 2,
        this.component.position.y - Scene.offsetY - 50
      )
  }

  /**
   * Custom event handler code then a the character move end.
   */
  onMoveEnd() {
    if (Scene.action !== null) Scene.action()
  }

  /**
   * @param {Array} path
   */
  set path(path) {
    this.stop()
    this._path = path
    this.movePath()
  }

  get path() {
    return this._path
  }
}

/**
 * Static properties
 */
Character.characterCount = 0
