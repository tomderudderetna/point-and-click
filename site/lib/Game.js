'use strict'

/**
 * Represents the  global application.
 * @param {object} canvas - The canvas object.
 * @param {object} options
 */
class Game {
  /**
   * @constructor
   * @param {object} canvas - The canvas object.
   * @param {object} options
   */
  constructor(canvas, options) {
    canvas = $id(canvas) // canvas
    Game.canvas = canvas // canvas
    this.ctx = canvas.getContext('2d') // context
    this.scene = new SceneManager(this) // scenes
    this.display = new DisplayManager(this.ctx) // display
    this.cursor = new CursorManager(this.ctx) // cursor
    this.muted = CookieManager.getState('muted') // audio
    this.events = new EventManager(this) // events

    // load options
    if (options) {
      const { scenes, onSceneAdded, onMuteChange, cursors } = options

      if (onSceneAdded) this.onSceneAdded = onSceneAdded
      if (onMuteChange) this.onMuteChange = onMuteChange
      if (scenes) {
        for (let i = 0; i < scenes.length; i++) {
          this.scene.add(scenes[i])
        }
      }
      if (cursors) {
        cursors.default && this.cursor.set(cursors.default, CURSOR_DEFAULT)
        cursors.left && this.cursor.set(cursors.left, CURSOR_LEFT)
        cursors.right && this.cursor.set(cursors.right, CURSOR_RIGHT)
        cursors.top && this.cursor.set(cursors.top, CURSOR_TOP)
        cursors.bottom && this.cursor.set(cursors.bottom, CURSOR_BOTTOM)
      }
    }

    // DOM
    const btns = document.querySelectorAll('[data-pac-mute]')

    for (let i = 0; i < btns.length; i++) {
      const btn = btns[i]

      btn.addEventListener('click', () => {
        this.toggleAudio()
      })
    }
  }

  /**
   * Mute / Unmute audio
   */
  toggleAudio() {
    this.muted = !this.muted
    CookieManager.set('muted', this.muted, 30)
    this.onMuteChange(this.muted)
    this.scene.updateStateAudio()
  }

  /**
   * Custom event handler code then a scene has added in the application
   */
  onSceneAdded() {
    return
  }

  get onMuteChange() {
    return this._onMuteChange || (() => {})
  }

  set onMuteChange(f) {
    this._onMuteChange = f
    this._onMuteChange(this.muted)
  }
}

/**
 * Static properties
 */
Game.canvas = null
