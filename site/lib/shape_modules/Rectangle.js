'use strict';

/**
 * Represents a Rectangle.
 */
class Rectangle {

    /**
     * Returns if a specified position occurs within a rectangle.
     * @param {Vector} position - The position to seek.
     * @param {object} coordinates - The coordinates.
     */
    static contains(position, coordinates) {
        return position.x > coordinates.x &&
            position.x < coordinates.x + coordinates.w &&
            position.y > coordinates.y &&
            position.y < coordinates.y + coordinates.h
    }
}