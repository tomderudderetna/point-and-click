'use strict'

const CURSOR_DEFAULT = 0
const CURSOR_TOP = 1
const CURSOR_RIGHT = 2
const CURSOR_BOTTOM = 3
const CURSOR_LEFT = 4

/**
 * Manage cursors.
 * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
 */
class CursorManager {
  /**
   * @constructor
   * @param {CanvasRenderingContext2D} ctx - The canvas 2D context.
   */
  constructor(ctx) {
    CursorManager.ctx = ctx // get context
    // hide browser cursor
    Game.canvas.style.cursor = 'none'
    Game.canvas.style.userSelect = 'none'
    // default cursors of the application
    this.cursors = {
      default: new Sprite('lib/resources/default_cursor.png', 0, 0, 50, 50),
      top: new Sprite('lib/resources/default_cursor_up.png', 0, 0, 50, 50),
      bottom: new Sprite('lib/resources/default_cursor_down.png', 0, 0, 50, 50),
      left: new Sprite('lib/resources/default_cursor_left.png', 0, 0, 50, 50),
      right: new Sprite('lib/resources/default_cursor_right.png', 0, 0, 50, 50)
    }
    this.cursorSelected = this.cursors.default
  }

  /**
   * Initialize a cursor
   * @param {Component} component - The cursor to add.
   * @param {number} type - The type of cursor (top, right, bottom, left).
   */
  set(component, type) {
    if (component && type !== undefined) {
      switch (type) {
        case CURSOR_DEFAULT:
          this.cursors.default.src = component.src
          this.cursors.default.width = component.width
          this.cursors.default.height = component.height
          break
        case CURSOR_TOP:
          this.cursors.top.src = component.src
          this.cursors.top.width = component.width
          this.cursors.top.height = component.height
          break
        case CURSOR_RIGHT:
          this.cursors.right.src = component.src
          this.cursors.right.width = component.width
          this.cursors.right.height = component.height
          break
        case CURSOR_BOTTOM:
          this.cursors.bottom.src = component.src
          this.cursors.bottom.width = component.width
          this.cursors.bottom.height = component.height
          break
        case CURSOR_LEFT:
          this.cursors.left.src = component.src
          this.cursors.left.width = component.width
          this.cursors.left.height = component.height
          break
      }
    }
  }

  /**
   * Draw a cursor.
   * @param {Component} cursor - The cursor to drawing.
   * @param {Vector} position - The position.
   */
  static draw(cursor, position) {
    CursorManager.ctx.drawImage(cursor, position.x, position.y, cursor.width, cursor.height)
  }
}

/**
 * Static properties
 */
CursorManager.ctx = null
