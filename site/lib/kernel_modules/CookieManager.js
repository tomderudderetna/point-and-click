'use strict';

/**
 * Manage cookies.
 */
class CookieManager {

    /**
     * Set cookies.
     * @param {string} name - The name of the cookie.
     * @param {string} cvalue - The value of the cookie.
     * @param {number} exdays - The number of days until the cookie should expire .
     */
    static set(name, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = 'expires=' + d.toUTCString();
        document.cookie = name + '=' + cvalue + ';' + expires + ';path=/';
    }

    /**
     * Get cookies.
     * @param {string} name - The name of the cookie.
     */
    static get(name) {
        var name = name + '=';
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return '';
    }

    /**
     * Get state of a binarie cookies.
     * @param {string} name - The name of the cookie.
     */
    static getState(name) {
        const state = CookieManager.get(name);
        return state === '' ? false : state === 'true';
    }
}