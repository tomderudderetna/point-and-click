'use strict';

/**
 * Represents a audio media.
 * @param {string} name - The name of the audio.
 * @param {string} src - The address of the resource.
 */
class Sound extends Audio {

    /**
     * @constructor
     * @param {string} name - The name of the audio.
     * @param {string} src - The address of the resource.
     */
    constructor(name = 'sound ' + Sound.soundCount, src) {
        super();

        this.state = false;
        this.name = name;
        this.onloadeddata = () => this.state = true;
        this.src = src;
        Sound.soundCount++;
    }

}

/**
 * Static properties
 */
Sound.soundCount = 0;