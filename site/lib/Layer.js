'use strict'

/**
 * Represents a Layer.
 * @param {object} options
 * @param {string} options.name - The name of the layer.
 */
class Layer {
  /**
   * @constructor
   * @param {object} options
   * @param {string} options.name - The name of the layer.
   */
  constructor({ name = 'name' + Layer.layerCount, zIndex = 0, components = [], characters = [] }) {
    this.name = name
    this.zIndex = zIndex
    this.components = []
    this.characters = []
    Layer.layerCount++

    for (let i = 0; i < components.length; i++) {
      const component = components[i]
      this.addComponent(component[0], component[1])
    }
    for (let i = 0; i < characters.length; i++) {
      this.addCharacter(characters[i])
    }
  }

  /**
   * Add a component in the layer
   * @param {string} name - The name of the component.
   * @param {component} component - The component to add.
   */
  addComponent(name, component) {
    component.name = name
    this.components.push(component)
  }

  /**
   * Add a character in the layer
   * @param {component} character - The name of the character.
   */
  addCharacter(character) {
    this.characters.push(character)
    this.characters.sort((characterA, characterB) => {
      return characterA.zIndex - characterB.zIndex
    })
  }
}

/**
 * Static properties
 */
Layer.layerCount = 0
