'use strict'

const CONF_HIDE_AREA = true

document.addEventListener('DOMContentLoaded', () => {
  // LEVEL 1 create assets (audio, commponents, characters, areas)
  const audiosList = new Map([
    ['ambiance street', new Sound('background music', '/assets/audio/ambiant_street.ogg')],
    ['step', new Sound('footstep', '/assets/audio/footstep.ogg')]
  ])
  const componnetsList = new Map([
    ['mur brique', new Sprite('/assets/img/street/wall_1.png', 0, 0)],
    ['lumiere jaune', new Sprite('/assets/img/street/ligth.png', 0, 720)],
    ['lampadaire', new Sprite('/assets/img/street/floorlamps.png', 0, 720)],
    ['ombre', new Sprite('/assets/img/street/shadows.png', 0, 0)],
    ['mur droite zoom', new Sprite('/assets/img/street/wall_right.png', 2607, 720)],
    ['mur brique', new Sprite('/assets/img/street/wall_1.png', 0, 0)],
    ['men', new Sprite('/assets/img/street/men.png', 625, 1020, 90, 275)],
    ['men2', new Sprite('/assets/img/street/men.png', 1925, 1020, 90, 275)],
    ['walk', new AnimatedSprite('/assets/img/character/walk.png', 2420, 1050, 2752, 600, 2, 16)],
    ['walk2', new AnimatedSprite('/assets/img/character/walk2.png', 2420, 1050, 2752, 600, 2, 16)]
  ])
  const charactersList = new Map([
    ['men left', new Character('men left', componnetsList.get('men'))],
    ['men right', new Character('men right', componnetsList.get('men2'))],
    [
      'sorcered',
      new Character('sorcered', componnetsList.get('walk'), {
        zIndex: 2,
        audios: [audiosList.get('step')]
      })
    ],
    [
      'sorcered-green',
      new Character('sorcered-green', componnetsList.get('walk2'), {
        zIndex: 99,
        audios: [audiosList.get('step')]
      })
    ]
  ])
  const areasList = new Map([
    [
      'area1',
      new RectArea('street area', WALKABLE_AREA, {
        x: 0,
        y: 1290,
        w: 2617,
        h: 150
      })
    ],
    [
      'area2',
      new RectArea('ladder area', WALKABLE_AREA, {
        x: 485,
        y: 200,
        w: 110,
        h: 1100
      })
    ],
    [
      'area3',
      new RectArea(
        'top ladder area',
        SWITCHABLE_AREA,
        {
          x: 455,
          y: 150,
          w: 160,
          h: 100
        },
        // add area event
        () => {
          app.scene.load(scene2)
          return false
        }
      )
    ],
    [
      'area4',
      new RectArea(
        'street right',
        SWITCHABLE_AREA,
        {
          x: 2000,
          y: 1290,
          w: 617,
          h: 150
        },
        () => {
          app.scene.load(scene3)
          return false
        }
      )
    ],
    [
      'area5',
      new RectArea(
        'street area',
        SWITCHABLE_AREA,
        {
          x: 0,
          y: 1290,
          w: 2617,
          h: 150
        },
        () => {
          alert('FIN')
          return false
        }
      )
    ],
    [
      'area-test-1',
      new RectArea('street area', WALKABLE_AREA, {
        x: 0,
        y: 1290,
        w: 2617,
        h: 150
      })
    ],
    [
      'area-test-2',
      new RectArea('street area', WALKABLE_AREA, {
        x: 2300,
        y: 1200,
        w: 150,
        h: 150
      })
    ]
  ])

  // LEVEL 2 create layers
  const layersList = new Map([
    [
      'background wall',
      new Layer({
        components: [
          ['mur brique', componnetsList.get('mur brique')],
          ['lumiere jaune', componnetsList.get('lumiere jaune')]
        ]
        // zIndex: 0
      })
    ],
    [
      'mens',
      new Layer({
        components: [['lampadaire', componnetsList.get('lampadaire')]],
        zIndex: 1,
        characters: [charactersList.get('men left'), charactersList.get('men right')]
      })
    ],
    [
      'shadows',
      new Layer({
        components: [
          ['ombre', componnetsList.get('ombre')],
          ['mur droite zoom', componnetsList.get('mur droite zoom')]
        ],
        zIndex: 3
      })
    ],
    ['layer4', new Layer({ components: [['mur brique', componnetsList.get('mur brique')]] })]
  ])

  // create characters
  const menLeft = charactersList.get('men left')
  const menRight = charactersList.get('men right')
  // create main character
  // create areas
  const area1 = areasList.get('area1')
  const area2 = areasList.get('area2')
  const area3 = areasList.get('area3')
  const area4 = areasList.get('area4')
  // create layers
  const layer1 = layersList.get('background wall')
  const layer2 = layersList.get('mens')
  const layer3 = layersList.get('shadows')
  const layer4 = layersList.get('layer4')

  // LEVEL 3 create scenes
  const scene1 = new Scene('night', 2700, 1440, {
    // add layers in scene
    layers: [layer1, layer2, layer3],
    // add areas in scene
    areas: [area1, area2, area3],
    // add player character in scenes
    audio: [audiosList.get('ambiance street')],
    player: charactersList.get('sorcered')
  })
  const scene2 = new Scene('day', 2700, 1440, {
    layers: [layer1, layer2],
    areas: [area1],
    player: charactersList.get('sorcered')
  })
  const scene3 = new Scene('end', 2700, 1440, {
    areas: [area3],
    player: charactersList.get('sorcered')
  })
  const scene4 = new Scene('test', 2700, 1440, {
    layers: [layer3],
    areas: [
      areasList.get('area-test-1'),
      areasList.get('area-test-2'),
      areasList.get('area-test-3')
    ],
    player: charactersList.get('sorcered-green')
  })

  // LEVEL 4 story script
  // add click event
  menLeft.onclick = e => {
    menLeft.talk("Tu dois grimpe à l'echelle")
  }
  menRight.onclick = e => {
    const text = [
      'Ne me parle pas !',
      'Degage !',
      'Tu veux ma photo ?!',
      'Laisse moi tranquille !',
      "J'ai pas d'argent, part !",
      'Fiche moi la paix !'
    ]
    menRight.talk(text[Math.floor(Math.random() * text.length)])
  }

  // create app
  const app = new Game('canvas', {
    // add scenes in app
    scenes: [scene1, scene2, scene3, scene4],
    // add events handle
    onSceneAdded: onSceneAdded,
    onMuteChange: onMuteChange,
    cursors: {
      default: new Sprite('/assets/img/cursors/cursor.png', 0, 0, 40, 53),
      left: new Sprite('/assets/img/cursors/cursor_l.png', 0, 0, 48, 39),
      right: new Sprite('/assets/img/cursors/cursor_r.png', 0, 0, 48, 39),
      top: new Sprite('/assets/img/cursors/cursor_t.png', 0, 0, 39, 48),
      bottom: new Sprite('/assets/img/cursors/cursor_b.png', 0, 0, 39, 48)
    }
  })

  // autoload first scene
  app.scene.load(scene1)

  function onSceneAdded(scene) {
    const lstScene = $id('list-right')
    const btn = document.createElement('a')

    btn.textContent = scene && scene.name
    btn.classList.add('btn', 'btn-lg', 'btn-stroke', 'btn-ligth', 'center')
    btn.addEventListener('click', () => app.scene.load(scene))

    lstScene.insertBefore(btn, lstScene.lastChild.previousSibling)
  }

  function onMuteChange(muteState) {
    const btnMuteIcon = $id('btn-mute').getElementsByTagName('i')[0]

    btnMuteIcon.classList[muteState ? 'remove' : 'add']('fa-volume-up')
    btnMuteIcon.classList[muteState ? 'add' : 'remove']('fa-volume-off')
  }
})
