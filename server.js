'use strict'

const express = require('express')
const server = express()
const { ip, port } = require('./config.js')

server.use(express.static(__dirname + '/site'))

server.listen(port, ip, () => {
  console.log(`http://${ip}:${port}/`)
})
